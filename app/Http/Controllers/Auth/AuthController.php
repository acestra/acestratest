<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Input;
use Session;
use Auth;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    public function doLogin()
        {
        // validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

       
        if ($validator->fails()) {
           $result = array('status'=>'error','message'=>"validation Faild","id"=>'');
           
        } else {
             $userdata = array(
                'email'     => Input::get('email'),
                'password'  => Input::get('password')
            );

           $attempt =Auth::attempt($userdata);
           
            if ($attempt) {
                
                    $user = Auth::user()->id;  
                    $result = array('status'=>'success','message'=>"login successful","id"=>$user);
                  

            } else {        
                $result = array('status'=>'error','message'=>"Invalid User Details or Inactive user. Please Contact Admin","id"=>'');

            }
            return json_encode($result);
           }
 }
     protected function store_register()
        {

            $userdata = array('name'=>Input::get('name'),'email'=>Input::get('email'),'password'=>Input::get('password'),'password_confirmation'=>Input::get('password_confirmation'));
            
            print_r($userdata);
             $validator = Validator::make($userdata,array('name'=>'required','email'=>'required|email|unique:user','password'=>'required|confirmed','password_confirmation'=>'required'
                 ));
              if($validator->fails())
             {
            // $message = $validator->message();
             //return Redirect::to('add_admin_user')->withErrors($validator)->withInput(Input::all());
           // echo "Validation_Error";
            return redirect()->back()->withErrors($validator);
             
         }else{
             $password = bcrypt(Input::get('password'));
             $storedata = array('firstName'=>Input::get('name'),'email'=>Input::get('email'),'password'=>$password);
            
             $user = new User();
             $user->fill($storedata);
             $user->save();
            Session::flash('success', 'success'); 
                              // return Redirect::to('admin');
            echo "success";
         }
        }
       
}
