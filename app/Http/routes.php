<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::any('store_register', 'Auth\AuthController@store_register');




//website controller
Route::any('home', 'WebsiteController@index');
Route::any('get_register', 'WebsiteController@get_register');
Route::any('profile', 'WebsiteController@userprofile');
Route::any('doLogin', 'Auth\AuthController@doLogin');