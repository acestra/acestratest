<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReligionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('religion', function (Blueprint $table) {
                            $table->increments('id');
                            $table->integer('religion',false,true);
                            $table->integer('caste',false,true);
                            $table->integer('sub_caste',false,true);
                            $table->tinyInteger('willing_same_caste');
                            $table->tinyInteger('willing_other_caste');
                            $table->integer('gothram',false,true);
                            $table->integer('star',false,true);   
                            $table->integer('raasi',false,true);      
                            $table->tinyInteger('have_dosham');
                            $table->integer('userId',false,true);
                            $table->foreign('userId')->references('id')->on('user');

                        
           
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('religion');
    }
}
