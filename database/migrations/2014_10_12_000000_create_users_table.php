<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
                        $table->increments('id');
                        $table->string('created_for');
                        $table->string('firstName');
                        $table->string('lastName');
                        $table->date('dob');
                        $table->string('gender');
                        $table->string('realm',512);
                        $table->string('username');
                        $table->string('email');
                        $table->bigInteger('mobile');
                        $table->integer('height',false,true);
                        $table->integer('weight',false,true);
                        $table->string('password', 60);
                        $table->longText('credentials');
                        $table->longText('challenges');
                        $table->tinyInteger('emailVerified');
                        $table->string('verificationToken', 512);    
                        $table->string('status');
                        $table->dateTime('created');
                        $table->rememberToken();
                        $table->dateTime('lastUpdated');
           
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('user');
    }
}
