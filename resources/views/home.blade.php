
@extends('master')

@section('website_content')
<style type="text/css">
	.topLeft{
		margin-top:16px;margin-left:4px
	}
</style>
<script src="<?php echo url(); ?>/web/js/homecntl.js"></script>
<div layout="row"  flex ng-controller="homecntl">
	<md-sidenav layout="column" class="md-sidenav-left md-whiteframe-z2" hide-gt-md hide-gt-sm  md-component-id="left" md-is-locked-open="$mdMedia('gt-sm')">
    </md-sidenav>
	  <div layout="column" flex id="content">
		<md-content  flex class="md-padding">
		  <div class="container top">
			<md-card>
			    <img src="<?php echo url(); ?>/web/img/wed.jpg" class="md-card-image img_width" alt="image caption">
			</md-card>
			  <div layout="row" layout-gt-sm="column" >
				<div class="tag" flex="40" flex-sm="80" flex-gt-sm="100" flex-gt-md="40" >
				  <md-whiteframe class="md-whiteframe-z3 " layout layout-align="center center">
					 <div class="tabsdemoDynamicHeight">
						<md-content class="md-padding">
						   <md-tabs md-dynamic-height md-border-bottom>
							<md-tab label="Register">
							  <md-content class="md-padding">
								<md-content layout-padding>
								   <form name="regForm" ng-submit="regSubmit()">
									 <md-content md-theme="docs-dark"  layout="row" layout-gt-sm="row" >
										<md-input-container class="">
												<label>Name</label>
												<input ng-model="reg.firstName" type="text" >
										</md-input-container>
										<md-input-container class="">
												<label>Email</label>
												<input ng-model="reg.email" type="email" >
										</md-input-container>
									</md-content>
									 <md-input-container>
								        <label>Profile Created for</label>
								        <md-select ng-model="reg.created_for" >
								          <md-option ng-value="t.title" data-ng-repeat="t in widget" ng-bind="t.title"></md-option>
								        </md-select>
								      </md-input-container>
								       <div layout layout-sm="column">
								       <div flex="30" class="topLeft"><label>Gender</label></div>
		                                  <md-input-container flex="70">
		                                      <md-radio-group ng-model="reg.gender" layout="row" >
												<md-radio-button value="Male" class="md-primary" reg.gender><label>Male</label></md-radio-button>
												<md-radio-button value="Female" ng-model="reg.gender"> <label>Female</label> 
											  </md-radio-button>
											</md-radio-group>
										  </md-input-container>
									  </div>
									   <div layout layout-sm="column">
									       <div flex="30" class="topLeft"><label>Date Of Birth</label></div>
									       <div flex="70"><input type="text" class="datepicker" ng-model="dob"> </div>
									      
									   </div>
									  <input type="text" ng-model="dobb">
						              <md-input-container class="pad_wed">
											<label>Enter Mobile Number:</label>
											<md-icon md-svg-src="img/icons/ic_phone_24px.svg"></md-icon>
											<input ng-model="reg.mobile" type="text" placeholder="Phone Number" >
									   </md-input-container>
						               <md-input-container>
						                     <label>Religion</label>
						                     <md-select ng-model="reg.religion" >
												<md-option ng-value="t.title" data-ng-repeat="t in religion" ng-bind="t.title"></md-option>
											 </md-select>
	                                   </md-input-container>
	                                   <div layout layout-sm="column">
	                                         <md-input-container class="">
												<label>User Name</label>
												<input ng-model="reg.userName" type="text">
												</md-input-container>
											<md-input-container>
												<label>Password</label>
												<input type="Password" name="password"  ng-model="reg.password" placeholder="Your Password" >
											</md-input-container>
	                                   </div>
	                                    <md-input-container>
	                                        <md-checkbox model="test"/><label>I Agree in Terms and Conditions</label></md-checkbox>
	                                    </md-input-container>
	                                     <md-input-container>
	                                          <md-button class="md-raised md-warn" ng-disabled="regForm.$invalid">Register</md-button>
	                                     </md-input-container>
	                                  </form>
	                                </md-content>
	                              </md-content>
	                           </md-tab>
	                            <md-tab label="Login"  flex="100">
									<md-content class="md-padding">
										<md-content layout-padding >
											<form  ng-submit="loginForm()" name="userForm">
												
												    <div layout layout-sm="row">
                                                         <md-input-container flex >
															<label>Email</label>
														   <input ng-model="user.email" type="text" required>
													    </md-input-container>
                                                         <md-input-container flex>
															<label>Password</label>
															<input ng-model="user.password" type="password" required>
														</md-input-container>
												    
														</div>
														<md-input-container>
					                                        <md-checkbox model="test"/><label>Remember Me</label></md-checkbox>
					                                    </md-input-container>
														<div layout layout-sm="row">
															<md-input-container flex>
                                                        	   <md-button class="md-raised md-warn" ng-disabled="userForm.$invalid">Login</md-button>
												    		</md-input-container>
												    		<md-input-container flex>
												    		<a href="" class="topLeft">Forgot Password?</a>
												    		</md-input-container>
												    	</div>
												    	<div layout layout-sm="row">
												    	   <span ng-bind="result.status" flex="20"></span>
												    	   <span ng-bind="result.message" flex="60"></span>
												    	</div>
												    	
												</form>
										</md-content>
									</md-content>
						       </md-tab>
						    </md-tabs>
						</md-content>
						</div>
						</md-whiteframe>
						</div>
					</div>
				</div>
			</md-content>
		</div>
	</div>

@endsection
<!-- resources/views/auth/register.blade.php -->
