<html lang="en" ng-app="StarterApp">
  <head>
    <link rel="stylesheet" href="<?php echo url(); ?>/web/css/angular-material.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic">
    <link rel="stylesheet" href="<?php echo url(); ?>/web/css/materialize.min.css">
    <meta name="viewport" content="initial-scale=1" />
    <link rel="stylesheet" href="<?php echo url(); ?>/web/css/style.css">
  </head>
  <body layout="column" >
  <nav class="teal lighten-2" ng-controller="AppCtrl">
  <div class="nav-wrapper container">
    <a href="<?php echo url();?>/home" class="brand-logo"><img ng-src="@{{ logoImage }}" style="width:195px;padding-top:10px" /></a>
    <a href="" data-activates="mobile-demo" class="button-collapse">
      <img src="web/img/menu.png" style="width:60px;padding:10px" />
    </a>
    <ul class="right hide-on-med-and-down" >
      <li ng-repeat="item in menus"><a ng-href="@{{ item.menu_link}}" ng-bind="item.menu_name"></a></li>
      <?php 
       
       if(isset(Auth::user()->id)){
       	?>
       	<li><a href="auth/logout">Logout</a></li>
       	<?php
       }
      ?>
    </ul>

    <ul class="side-nav" id="mobile-demo" >
      <li ng-repeat="item in menus"><a ng-href="@{{ item.menu_link}}" ng-bind="item.menu_name"></a></li>
    </ul>
  </div>
</nav>
	

<!-- Angular Material Dependencies -->
<script src="<?php echo url(); ?>/web/js/angular.min.js"></script>
<script src="<?php echo url(); ?>/web/js/angular-animate.min.js"></script>
<script src="<?php echo url(); ?>/web/js/angular-aria.min.js"></script>
<script src="<?php echo url(); ?>/web/js/app.js"></script>
<script src="<?php echo url(); ?>/web/js/angular-material.min.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/web/js/jquery-2.1.1.min.js"></script>
<script src="<?php echo url(); ?>/web/js/materialize.min.js"></script>
<script>
   $( document ).ready(function() {
       $('.datepicker').pickadate({
          format: 'mm/dd/yyyy',
          selectMonths: true, // Creates a dropdown to control month
          selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        $(".button-collapse").sideNav();
    });
        </script>
        @yield('website_content')
</body>



</html>