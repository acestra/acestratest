@if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
<form method="POST" action="<?php echo url();?>/get_register">
    {!! csrf_field() !!}

    <div>
        Name
        <input type="text" name="firstName" value="">
    </div>
    
    <div>
        created_for
        <input type="text" name="created_for" value="brother">
    </div>
    <div>
        gender
        <input type="text" name="gender" value="male">
    </div>
     <div>
        mobile
        <input type="text" name="mobile" value="9874561230">
    </div>
     <div>
        Religion
        <input type="text" name="religion" value="1">
    </div>
    <div>
        Mother Tongue
        <input type="text" name="mother_tongue" value="1">
    </div>
     <div>
       dob
        <input type="text" name="dob" value="2015-10-10 00:00:00">
    </div>
    <div>
        living in
        <input type="text" name="living_in" value="1">
    </div>
    <div>
        Email
        <input type="email" name="email" value="">
    </div>
    
    <div>
        Password
        <input type="password" name="password">
    </div>

   

    <div>
        <button type="submit">Register</button>
    </div>
</form>